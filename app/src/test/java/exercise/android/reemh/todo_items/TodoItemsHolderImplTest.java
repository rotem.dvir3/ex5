package exercise.android.reemh.todo_items;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

public class TodoItemsHolderImplTest {
  @Test
  public void when_addingTodoItem_then_callingListShouldHaveThisItem(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());

    // test
    holderUnderTest.addNewInProgressItem("do shopping");

    // verify
    Assert.assertEquals(1, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_deletingTodoItem_then_callingListShouldnotHaveThisItem_1(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());

    // test
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.deleteItem(holderUnderTest.cur_items_proccessing.get(0));

    // verify
    Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_4(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");



    // verify
    Assert.assertEquals(3, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_3(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.deleteItem(holderUnderTest.cur_items_proccessing.get(0));
    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));

    // verify
    Assert.assertEquals(3, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_5(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");


    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));

    // verify
    Assert.assertEquals(2, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_6(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");

    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));
    holderUnderTest.deleteItem(holderUnderTest.cur_items_done.get(0));

    // verify
    Assert.assertEquals(0, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_7(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");

    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));

    // verify
    Assert.assertEquals(1, holderUnderTest.getCurrentItems().size());
  }
  @Test
  public void when_marking_item_done_8(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");


    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));

    // verify
    Assert.assertEquals(5, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_9(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("do shopping");


    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));
    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));


    // verify
    Assert.assertEquals(3, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void when_marking_item_done_10(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");

    holderUnderTest.markItemDone(holderUnderTest.cur_items_proccessing.get(0));
    holderUnderTest.markItemInProgress(holderUnderTest.cur_items_done.get(0));


    // verify
    Assert.assertEquals(1, holderUnderTest.getCurrentItems().size());
  }

}