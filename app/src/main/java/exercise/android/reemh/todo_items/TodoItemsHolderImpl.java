package exercise.android.reemh.todo_items;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.mockito.internal.matchers.Null;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TodoItemsHolderImpl implements TodoItemsHolder {
  ArrayList<TodoItem> cur_items_proccessing;
  ArrayList<TodoItem> cur_items_done;
  int counter;
  private static Context myContext =null;
  private static  SharedPreferences pref;
  private static final MutableLiveData<List<TodoItem>> myMutable = new MutableLiveData<>();
  public static final LiveData<List<TodoItem>> myLiveDate=myMutable;

  @RequiresApi(api = Build.VERSION_CODES.O)
  protected TodoItemsHolderImpl(Context context){
    cur_items_proccessing =new ArrayList<>();
    cur_items_done = new ArrayList<>();
    myContext=context;
    pref = context.getSharedPreferences("local_db_app",Context.MODE_PRIVATE);
    set_mutable();

  }



  @RequiresApi(api = Build.VERSION_CODES.O)
  private void set_mutable(){
    for(String name:pref.getAll().keySet()){
      add_item(new TodoItem(pref.getString(name,null)));
    }
    Collections.sort(cur_items_proccessing);
    Collections.sort(cur_items_done);
    myMutable.setValue(getCurrentItems());




  }

  private void add_item(TodoItem item){
    if(item.isDone){
      cur_items_done.add(item);
      return;
    }
    cur_items_proccessing.add(item);

  }

  @Override
  public TodoItem getItem(int id){
    for (TodoItem todo:cur_items_proccessing) {
      if (todo.id==id){
        return todo;

      }

    }
    for (TodoItem todo:cur_items_done) {
      if (todo.id==id){
        return todo;

      }

    }
  return null;
  }

  public LiveData<List<TodoItem>> getLiveData() {
    return myLiveDate;
  }

  @Override
  public ArrayList<TodoItem> getCurrentItems() {
    ArrayList<TodoItem> newList = new ArrayList<>(cur_items_proccessing);
    newList.addAll(cur_items_done);
    return newList; }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void addNewInProgressItem(String description) {
    boolean inlist =false;
    for(TodoItem a :cur_items_done){
      if (a.TodoName.equals(description)){
        cur_items_done.remove(a);
        cur_items_proccessing.add(a);
        inlist=true;

        break;
      }
    }
   if (!inlist){
     TodoItem newitem = new TodoItem(description,counter);
     cur_items_proccessing.add(newitem);
   counter++;
     SharedPreferences.Editor editor = pref.edit();
     editor.putString(String.valueOf(newitem.id),newitem.toString());
     editor.apply();
     myMutable.setValue(getCurrentItems());
   }

    Collections.sort(cur_items_proccessing);



  }


  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void markItemDone(TodoItem item) {
    if (!item.isDone){
      cur_items_proccessing.remove(item);
      cur_items_done.add(item);
      item.MarkItemDone();
      Collections.sort(cur_items_done);
      SharedPreferences.Editor editor = pref.edit();
      editor.putString(String.valueOf(item.id),item.toString());
      editor.apply();
      myMutable.setValue(getCurrentItems());

    }





  }
  public void sendBroadCastDBchanges(){
    Intent brod = new Intent("db_changed");
    brod.putExtra("new_list", (Parcelable) this.getCurrentItems());
    myContext.sendBroadcast(brod);
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void editTodo(TodoItem item,String newName) {

    item.ChangeName(newName);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(String.valueOf(item.id),item.toString());
    editor.apply();
    myMutable.setValue(getCurrentItems());



  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void markItemInProgress(TodoItem item) {
    if (item.isDone){
      cur_items_done.remove(item);
      cur_items_proccessing.add(item);
    }

    item.markItemInProgress();
    Collections.sort(cur_items_proccessing);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(String.valueOf(item.id),item.toString());
    editor.apply();
    myMutable.setValue(getCurrentItems());


  }

  @Override
  public void deleteItem(TodoItem item) {
    if (item.isDone){
      cur_items_done.remove(item);
    }
    else{
      cur_items_proccessing.remove(item);

    }
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(String.valueOf(item.id),item.toString());
    editor.apply();
    myMutable.setValue(getCurrentItems());


  }


}
