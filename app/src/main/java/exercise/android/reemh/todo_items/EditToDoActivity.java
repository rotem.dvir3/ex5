package exercise.android.reemh.todo_items;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class EditToDoActivity extends AppCompatActivity {
    public TodoItemsHolder database= null;
    private TodoItem myItem;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (database==null){
            database=ToDoAPP.getInstance().getDatabase();
        }
        Intent intentToOpen = getIntent();
        myItem = database.getItem(intentToOpen.getIntExtra("cur_item",0));
        setContentView(R.layout.edit_item_view);

        Button task_status = findViewById(R.id.STATUS);
        EditText taskName = findViewById(R.id.toDoName);
        TextView created_view = findViewById(R.id.created);
        TextView edited_view = findViewById(R.id.edited);
        FloatingActionButton edit_butt = findViewById(R.id.finishedit);
        edit_butt.setEnabled(false);
        taskName.setEnabled(true);



        taskName.setHint(myItem.TodoName); // cleanup text in edit-text
        task_status.setEnabled(true);
        task_status.setText(myItem.getStatus());// set edit-text as enabled (user can input text)
        created_view.setText(myItem.getStartDate());
        edit_butt.setOnClickListener(v -> {
                    if (taskName.getText().toString().length() != 0) {
                        database.editTodo(myItem,taskName.getText().toString());
                        taskName.setEnabled(true);
                        taskName.setHint(myItem.TodoName);
                        edited_view.setText(myItem.getEditDate());
                        hideKeyboard(this);



                    }
                }
        );
        edited_view.setText(myItem.getEditDate());

        taskName.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            public void afterTextChanged(Editable s) {
                edit_butt.setEnabled(true);

//                taskName.setText(myItem.TodoName);

                ///TODO the diffrenet


            }
        });
        task_status.setOnClickListener(v->{
            if (myItem.isDone){
                database.markItemInProgress(myItem);
            }
            else{
                database.markItemDone(myItem);
            }
            task_status.setText(myItem.getStatus());

        });





    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



}
