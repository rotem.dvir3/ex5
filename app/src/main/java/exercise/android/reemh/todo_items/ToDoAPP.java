package exercise.android.reemh.todo_items;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class ToDoAPP extends Application {
    public TodoItemsHolder getDatabase() {
        return database;
    }

    public static ToDoAPP getInstance() {
        return instance;
    }

    private static ToDoAPP instance =null;

    private static TodoItemsHolder database;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        if (database==null){
            database= new TodoItemsHolderImpl(this);


        }
        instance=this;
    }
}
