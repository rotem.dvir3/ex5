package exercise.android.reemh.todo_items;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;


public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHolder> {

    private TodoItemsHolder localDataSet;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    static class TodoViewHolder extends RecyclerView.ViewHolder {
        CheckBox doneButton;
        Button deleteButton;
        Button editButton;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);
            doneButton = (CheckBox) itemView.findViewById(R.id.doneButton);
            deleteButton = itemView.findViewById(R.id.deleteSingelTask);
            editButton = itemView.findViewById(R.id.editTodoName);
        }
    }


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param curTasks א containing the data to populate views to be used
     *                 by RecyclerView.
     */
    public TodoAdapter(TodoItemsHolder curTasks) {

        localDataSet = curTasks;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_todo_item, viewGroup, false);

        return new TodoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        TodoItem cur_item = localDataSet.getCurrentItems().get(position);
        holder.doneButton.setText(cur_item.TodoName);
        holder.doneButton.setChecked(cur_item.isDone);

        holder.doneButton.setOnClickListener(v -> {
            //My code...
            if (cur_item.isDone) {
                localDataSet.markItemInProgress(cur_item);
            }
         else {
            localDataSet.markItemDone(cur_item);
            //change view
        }

            notifyDataSetChanged();
        });


        holder.deleteButton.setOnClickListener(v ->

    {
        this.localDataSet.deleteItem(cur_item);
        this.notifyDataSetChanged();
    }
    );

    holder.editButton.setOnClickListener(v->{
        Intent toSend = new Intent(v.getContext(),EditToDoActivity.class);
        toSend.putExtra("cur_item",cur_item.id);
        v.getContext().startActivity(toSend);
        notifyDataSetChanged();

    });
}


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.getCurrentItems().size();
    }


}




