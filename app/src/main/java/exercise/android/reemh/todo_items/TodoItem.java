package exercise.android.reemh.todo_items;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class TodoItem implements  Comparable<TodoItem> ,Serializable {
    public String TodoName;
    public boolean isDone;
    public int id;
    public LocalDateTime crationDate;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getEditDate() {
        LocalDateTime fromDateTime = editDate;
        LocalDateTime toDateTime =java.time.LocalDateTime.now() ;

        LocalDateTime tempDateTime = LocalDateTime.from( fromDateTime );

        long years = tempDateTime.until( toDateTime, ChronoUnit.YEARS );
        tempDateTime = tempDateTime.plusYears( years );

        long months = tempDateTime.until( toDateTime, ChronoUnit.MONTHS );
        tempDateTime = tempDateTime.plusMonths( months );

        long days = tempDateTime.until( toDateTime, ChronoUnit.DAYS );
        tempDateTime = tempDateTime.plusDays( days );

        long hours = tempDateTime.until( toDateTime, ChronoUnit.HOURS );
        tempDateTime = tempDateTime.plusHours( hours );
        long minutes = tempDateTime.until( toDateTime, ChronoUnit.MINUTES );
        tempDateTime = tempDateTime.plusMinutes( minutes );


        if(years<1 && months<1 && days<1 && hours<1){
            return "was edit "+minutes+" minutes ago";
        }
        else if (years<1 && months<1 && days<1){
            return "was edit today at hour:"+ editDate.getHour();

        }
        Date in = new Date();
        Date out = Date.from(editDate.atZone(ZoneId.systemDefault()).toInstant());


       return  "was edit at date "+ out.toString();

    }

    public LocalDateTime editDate;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getStartDate() {
        Date in = new Date();
        Date out = Date.from(crationDate.atZone(ZoneId.systemDefault()).toInstant());

        return "was created at "+ out.toString() ;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TodoItem(String name, int id){
        isDone = false;
        TodoName=name;
        this.id=id;
        crationDate = java.time.LocalDateTime.now();
        editDate= java.time.LocalDateTime.now();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TodoItem(){
        isDone = false;
        TodoName="no_name";
        id=0;
        crationDate = java.time.LocalDateTime.now();
        editDate= java.time.LocalDateTime.now();


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TodoItem(String reps){
        String[] param =reps.split("\\$");
        TodoName=param[0];
        id=Integer.parseInt(param[1]);
        isDone = Boolean.parseBoolean(param[2]);
        crationDate = LocalDateTime.parse(param[3]);
        editDate = LocalDateTime.parse(param[4]);


    }

    @RequiresApi(api = Build.VERSION_CODES.O)

    void markItemInProgress(){
        if (isDone){
            isDone=false;
            editDate= java.time.LocalDateTime.now();
        }


    }
    @RequiresApi(api = Build.VERSION_CODES.O)

    void MarkItemDone(){
        if(!isDone){
            isDone =true;
            editDate= java.time.LocalDateTime.now();
        }


    };

    @RequiresApi(api = Build.VERSION_CODES.O)
    void ChangeName(String newName){
        TodoName=newName;
        editDate = java.time.LocalDateTime.now();

    }

    @Override
    public int compareTo(TodoItem o) {
        int compareQuantity = o.id;

        //ascending order
        return this.id - compareQuantity;
    }
    @NonNull
    @Override
    public String toString(){
        StringBuilder newString = new StringBuilder();
        newString.append(TodoName);
        newString.append("$");
        newString.append(this.id);
        newString.append("$");
        newString.append(this.isDone);
        newString.append("$");
        newString.append(this.crationDate);
        newString.append("$");
        newString.append(this.editDate);
        return newString.toString();
    }
     public String getStatus(){
        if (isDone){
            return "DONE!";
        }
        return "IN PROGRESS...";
     }}



